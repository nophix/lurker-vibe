# lurker.vibe
Collects Spotify presences from Discord and sends them in batches to a given endpoint.

## Usage
Using Python:
```sh
# Copy and modify .env
cp .env.sample .env

# Install deps and run
python -m pip install -r requirements.txt
python lurker/bot.py
```
