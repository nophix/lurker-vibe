-- migrate:up
CREATE OR REPLACE TABLE spotify_albums (
	album_id BIGINT UNSIGNED auto_increment NOT NULL,
	artist varchar(100) NULL,
	name varchar(100) NULL,
	cover_url varchar(100) NULL,
	CONSTRAINT spotify_albums_PK PRIMARY KEY (album_id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

CREATE TABLE spotify_tracks (
	track_id varchar(100) NOT NULL,
	album_id BIGINT UNSIGNED NOT NULL,
	title varchar(100) NULL,
	duration INT NULL,
	CONSTRAINT spotify_tracks_PK PRIMARY KEY (track_id),
	CONSTRAINT spotify_tracks_FK FOREIGN KEY (album_id) REFERENCES spotify_albums(album_id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

CREATE TABLE spotify_plays (
	user_id BIGINT UNSIGNED NOT NULL,
	track_id varchar(100) NOT NULL,
	created_at TIMESTAMP NOT NULL,
	CONSTRAINT spotify_plays_PK PRIMARY KEY (user_id,created_at),
	CONSTRAINT spotify_plays_FK FOREIGN KEY (track_id) REFERENCES spotify_tracks(track_id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8mb4
COLLATE=utf8mb4_general_ci;

-- migrate:down
DROP TABLE spotify_plays;
DROP TABLE spotify_tracks;
DROP TABLE spotify_albums;
